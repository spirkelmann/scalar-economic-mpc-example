function [lb, ub] = bounds( k, options )
% bounds for day and night
lb_day = -2;
ub_day = 2;

lb_night = -0.5;
ub_night = 0.5;

sep = 24;   % number of separations of a day
k_normalized = mod(k, sep);

% t1 = sep/4;
% t2 = sep/3;
% t3 = 3/4 * sep;
% t4 = 5/6 * sep;

t1 = -1;
t2 = -1;
t3 = 1/2 * sep-1;
t4 = 1/2 * sep-1;

if (k_normalized <= t1) % first quarter of each day = night
    lb = lb_night;
    ub = ub_night;
elseif (k_normalized <= t2) % transient phase
    lb = 1/(t1 - t2) * ... 
        ((lb_night - lb_day) * k_normalized + lb_day * t1 - lb_night * t2);
    ub = 1/(t1 - t2) * ... 
        ((ub_night - ub_day) * k_normalized + ub_day * t1 - ub_night * t2);
elseif (k_normalized <= t3) % day
    lb = lb_day;
    ub = ub_day;
elseif (k_normalized <= t4) % second transient phase
    lb = 1/(t3 - t4) * ... 
        ((lb_day - lb_night) * k_normalized + lb_night * t3 - lb_day * t4);
    ub = 1/(t3 - t4) * ... 
        ((ub_day - ub_night) * k_normalized + ub_night * t3 - ub_day * t4);
else    % night
    lb = lb_night;
    ub = ub_night;
end

