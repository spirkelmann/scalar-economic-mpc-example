% Disturbance
% This function models the disturbance 
function w = w(k, options)
    w = -2*sin(k * pi/12) + options.a(k+1);
end