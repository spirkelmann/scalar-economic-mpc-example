% function for the system dynamics
% physical interpretation:
%   x - temperature of the building
%   u - heating/cooling
%   w - outside temperatures
function x = f( k, x, u, options )
    x = x + u + w(k, options);
end