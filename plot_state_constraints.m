% Plot the state constraints to the specified axis
% Input:
%   ax  - axis to plot to
%   ts  - time instances
%
% The function looks up the state constraints for each time index k in the
% time sequence ts and plots them to the given axis handle.
function [ output_args ] = plot_state_constraints(ax, sim, plot_options)
    ts = sim.ts;

    plot_range = plot_options.range;
    linewidth = plot_options.linewidth;

    % range for which to plot data
    tplot = [];
    for i = 1:length(ts)
        if ts(i) >= plot_range(1) && ts(i) <= plot_range(end)
            tplot = [tplot; ts(i)];
        end
    end

    cs = [];
    for k = 1:length(tplot)
        [lb, ub] = bounds(tplot(k));
        cs = [cs; lb ub];
    end

    plot(ax, tplot, cs(:,1),'color','r','linestyle','--', 'linewidth', linewidth)
    plot(ax, tplot, cs(:,2),'color','r','linestyle','--', 'linewidth', linewidth)

    lb = min(cs(:,1));
    ub = max(cs(:,2));
    ylim([lb - 0.1 * (ub-lb) ub + 0.1 * (ub-lb)]);
    set(gca,'TickLabelInterpreter','latex')
    xlim(plot_range)
    ylim([-2.5 2.5])
    box on;
end

