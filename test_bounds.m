close all;
lbs = []
ubs = []
n = 200;
for k = 0:n
    [l,u] = bounds(k);
    lbs = [lbs; l];
    ubs = [ubs; u];
end
plot(0:n, lbs); hold on;
plot(0:n, ubs)