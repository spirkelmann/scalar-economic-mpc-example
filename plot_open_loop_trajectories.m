% Plot the open loop control trajectories to the specified axis
% Input:
%   ax  - axis to plot to
%   ts_ol  - time instances
%   xs_ol  - controls
% The function plots all of the given open loop trajectories into the given
% axis handle.
function plot_open_loop_trajectories(ax, ts_ol, xs_ol )
    for i = 1:length(ts_ol(:,1))
        plot(ax, ts_ol(i,:), xs_ol(i,:),'color','r','linestyle',':')
    end
    set(gca,'TickLabelInterpreter','latex')
end

