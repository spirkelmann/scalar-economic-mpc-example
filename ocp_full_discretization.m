
% solve the optimal control problem using a full discretization scheme
%   k   - current time index
%   x0  - initial value at current time
%   options   - options for ocp (i.e. horizon length, etc.)
%
% The full discretization scheme treats both states and controls as
% optimization variables. In total there are 2*N optimization variables
% that correspond to the following states/controls:
%   z = [x0 u0 x1 u1 ... xN-1 uN-1]
%
% The system dynamic is implemented as a linear constraint to the 
% optimization problem. 
% The time-varying constraints for the state and the constraints for the
% control are implemented as simple lower and upper bounds (box
% constraints) of the optimization variable z.
function [u_ol, x_ol, fval,exitflag] = ocp_full_discretization(k, x0, options)
% initial value
z = zeros(2*options.N+1,1);

% no linear inequality constraints
A = [];
b = [];

% system dynamic as equality constraints
[Aeq, beq] = assemble_eq_constraints(k, x0, options);

% define state and control constraints
[lb, ub] = assemble_state_control_constraints(k, options);

if options.free_initial_value == 0
    lb(1) = x0;
    ub(1) = x0;
end

% set option to use derivative information
optim_options = optimset('Algorithm', 'interior-point', 'GradObj', 'on', 'Hessian', ...
    'user-supplied', 'HessFcn', @(z, lambda) hessian_objective(z, lambda, options), ...
    'Display', 'off');

% run optimization
[z,fval,exitflag, output, lambda, grad, hessian] = fmincon(@(z) objective(k,z, options), z, A, b, Aeq, beq, lb, ub, [], optim_options);

% extract optimal control sequence from result of the optimization
u_ol = z(2:2:end);
x_ol = z(1:2:end);

% check gradient
[J, gradJ] = objective(k, z, options);
H = hessian_objective( z, lambda, options );
n = norm(grad - gradJ);
%nh = norm(H - hessian)
end

% objective function
function [J, gradJ] = objective(k, z, options)
    N = options.N;
    
    J = 0;
    gradJ = zeros(2*N+1,1);
    
    for j = 0:N-1
        [l, gradl] = stagecost(k+j,z(2*j+1),z(2*j+2), options);
        J = J + l;
        gradJ(2*j+1:2*j+2) = gradl;
    end
    
    % final state (dummy control = 0)
    [l, gradl] = stagecost(k+N,z(2*N+1),0, options);
    J = J + l;
    gradJ(2*N+1) = gradl(1);
end

% The system dynamic is defined as an equality constraint for the 
% optimization problem.
% The matrix looks like this:
% A =   [ 1  1  -1 ...                          ]
%       [    0   1   1  -1 ...                  ]
%       [            0   1   1  -1 ...          ]
%       [                   ...                 ]
%       [                        1   1  -1   0  ]
% The rhs vector contains the following entries:
% b = [    w0   ]   % x1 for initial condition
%     [    w1   ]
%     [    w2   ]
%     [    ...  ]
%     [    wN-1 ]
function [Aeq, beq] = assemble_eq_constraints(k, x0, options)
    N = options.N;
    
    Aeq = zeros(N, 2 * N+1);
    for j = 0:N-1
       Aeq(j+1, 2*j+1) = 1;
       Aeq(j+1, 2*j+2) = 1;
       Aeq(j+1, 2*j+3) = -1;
    end
    Aeq = sparse(Aeq);  % convert to sparse matrix

    % rhs is time varying data and initial value of x0
    beq = zeros(N, 1);
    for j = 0:N-1
        beq(j+1) = -w(k + j, options);
    end
end

function [lb, ub] = assemble_state_control_constraints(k, options)
    N = options.N;
    
    lb = -Inf * ones(2 * N+1, 1);
    ub = Inf * ones(2 * N+1, 1);
    for j = 0:N
        % constraints for state
        [lb_x, ub_x] = bounds(k+j, options);
        lb(2*j+1) = lb_x;
        ub(2*j+1) = ub_x;

        % constraints for the control
        %lb(2*j+2) = -2;
        %ub(2*j+2) = 2;
    end
end