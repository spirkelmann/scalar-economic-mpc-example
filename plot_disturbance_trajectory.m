% Plot the disturbance sequence to the specified axis for the specified
% time range
% Input:
%   ax  - axis to plot to
%   ts  - time instances
%
% The function evaluates the data w(k) for each k in ts and plots the
% values.
function plot_disturbance_trajectory( ax, sim, plot_options )
    ts = sim.ts;
    
    plot_range = plot_options.range;
    color = plot_options.disturbance_color;
    linewidth = plot_options.linewidth;
    linestyle = '-';
    
    % extract plotting data
    wplot = [];
    tplot = [];
    for i = 1:length(ts)
        if ts(i) >= plot_range(1) && ts(i) <= plot_range(end)
            tplot = [tplot; ts(i)];
            wplot = [wplot; w(ts(i), sim.options)];
        end
    end

    plot(ax, tplot, wplot,'color',color, 'linewidth', linewidth, 'linestyle', linestyle)
    xlim(plot_range)
    ylim([-3 3])
    xlabel('Time $k$','interpreter','latex'); ylabel('$w(k)$','interpreter','latex');
    set(gca,'TickLabelInterpreter','latex')
    box on;
end