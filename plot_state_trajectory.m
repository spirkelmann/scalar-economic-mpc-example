% Plot the state sequence to the specified axis
% Input:
%   ax  - axis to plot to
%   ts  - time instances
%   us  - states
function plot_state_trajectory(ax, sim, plot_options)
    ts = sim.ts;
    xs = sim.xs;
    
    plot_range = plot_options.range;
    color = plot_options.state_color;
    linewidth = plot_options.linewidth;
    linestyle = plot_options.linestyle;
    
    % extract plotting data
    xplot = [];
    tplot = [];
    for i = 1:length(ts)
        if ts(i) >= plot_range(1) && ts(i) <= plot_range(end)
            tplot = [tplot; ts(i)];
            xplot = [xplot; xs(i)];
        end
    end
    
    plot(ax, tplot, xplot,'color',color, 'linewidth', linewidth, 'linestyle', linestyle)
    xlim(plot_range)
    ylim([-3 3])
    xlabel('Time $k$','interpreter','latex','FontSize',25); ylabel('$x(k)$','interpreter','latex','FontSize',25);
    set(gca,'TickLabelInterpreter','latex')
    box on;
end

