
% Kernfunktion zur Optimierung mit variablem Horizont und Anfangswert
function [umin,fval,exitflag] = ocp(k, N, x0)

% Startwert fuer Optimierer
u0 = zeros(N,1);

% lineare Nebenbedingungen fuer fmincon
A = [];
b = [];
Aeq = [];
beq = [];

%global lb_u;
%global ub_u;
%lb = lb_u * ones(1,N);
%ub = ub_u * ones(1,N);
lb = [];
ub = [];
% for i=0:N-1
%     [l,u] = bounds(k+i)
%     lb = [lb; l];
%     ub = [ub; u];
% end

options = optimset('Algorithm', 'interior-point', 'Display', 'off');

[umin,fval,exitflag, output] = fmincon(@(u) objective(k,N,x0,u), u0, A, b, Aeq, beq, lb, ub, @(u) constraints(k,N,x0,u), options);

% fprintf('   k  |      umin(k) |     J \n');         % Ausgabe
% fprintf('------|--------------|-------\n');
% for i=1:N
% 	fprintf(' %3d  | %+11.6f | %+11.6f\n',i,umin(i),fval);
% end

end

% Zielfunktion
function J = objective(k, N, x0, u)
    x = x0;
    
    open_loop = [x];

    J = 0;
    for j = 0:N-1
        J = J + stagecost(k+j,x,u(j+1));
        x = f(k+j,x,u(j+1));
        
        open_loop = [open_loop x];
    end
    
    %plot([k:k+N], open_loop);
end

% Laufende Kosten
function l = stagecost(k,x,u)
    global epsilon;
    [lb,ub] = bounds(k);
    x_ref = (lb + ub)/2;
	l = u^2 + epsilon * (x - x_ref)^2;
end 

% % Systemdynamik
% function y = system(k,x,u)
% 	y = (1 + (-1)^k)/2 + x + u;
% end 

% Nichtlineare Nebenbedingungen fuer fmincon
function [c, ceq] = constraints(k, N, x0, u)
    %global ub_x;
    %global lb_x;
    x = x0;
    c = zeros(2*N,1);
    for j = 0:N-1
        x = f(k+j,x,u(j+1));
        
        [lb_x, ub_x] = bounds(k+j+1);
        
        c(2*j+1) = x - ub_x;
        c(2*j+2) = lb_x - x;
    end    

    ceq = [];
end