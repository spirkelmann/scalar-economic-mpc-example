clf;
clear;
close all;
rng(0);

%% set up simulation options
options.epsilon = 0.0;      % weighting term for (x-x_ref) in cost function
options.free_initial_value = 0;
options.N = 10;
options.L = 96;
options.a = 0.5*rand(1000,1) - 0.25  % random variations of w


x0 = 0.0;   % initial value
t0 = 0.0;   % start time
Ns = [2:1:10];

%% run simulations
% -> sim_results

sim_results = [];
for i = 1:length(Ns)
    options.N = Ns(i);
    
    [ ts, xs, us, ts_ol, xs_ol, us_ol, Js, J_cl ] = loop_solve( x0, t0, options );
    
    sim.ts = ts;
    sim.xs = xs;
    sim.us = us;
    sim.ts_ol = ts_ol;
    sim.xs_ol = xs_ol;
    sim.us_ol = us_ol;
    sim.Js = Js;
    sim.J_cl = J_cl;
    sim.options = options;
    
    sim_results = [sim_results; sim];
end

%% compute optimal trajectory
options_opt = options;
options_opt.free_initial_value = 1;
options_opt.N = 200;
options_opt.L = 0;
[ ts, xs, us, ts_ol, xs_ol, us_ol, Js, J_cl ] = loop_solve( x0, t0, options_opt );
sim_opt.ts = ts;
sim_opt.ts = ts_ol;
sim_opt.xs = xs_ol;
sim_opt.us = us_ol;
sim_opt.Js = Js;
sim_opt.options = options_opt;
sim_opt.J_cl = zeros(1, options.L);
cl_sum = 0.0;
%  compute cumulative closed loop cost of optimal trajectory
ells = zeros(1, options_opt.N);
for k = 0:options_opt.N-1
    ells(k+1) = stagecost(k, sim_opt.xs(k+1), sim_opt.us(k+1), options);
    cl_sum = cl_sum + ells(k+1);
    sim_opt.J_cl(1,k+1) = cl_sum;
end    

%% plot the results
plot_options.range = [0 50];
plot_options.state = 1;
plot_options.control = 1;
plot_options.disturbance = 0;
plot_options.optimal_trajectory = 1;    % plot optimal trajectory and optimal cost
plot_options.linewidth = 2;
plot_options.linestyle = '-';
plot_options.folder = 'figures/';
plot_options.prefix = '';
plot_options.state_color = 'red';
plot_options.control_color = 'blue';
plot_options.disturbance_color = [0 0.5 0];

plot_options_opt = plot_options;
plot_options_opt.linestyle = '-.';
plot_options_opt.state_color = 'black';
plot_options_opt.control_color = 'black';
plot_options_opt.disturbance_color = [0 0.5 0];
plot_options_opt.cost_color = 'black';

fig1 = figure(1);
fig2 = figure(2);
fig3 = figure(3);
fig4 = figure(4);

for i = 1:length(sim_results)
    sim = sim_results(i);
    
    % trajectory plots
    figure(fig1);
    px = plot_options.state + plot_options.control + plot_options.disturbance;
    
    if plot_options.state == 1
        ax = subplot(px,1,1); box(ax, 'on'); hold(ax, 'off');
        plot_state_trajectory(ax, sim, plot_options); hold(ax, 'on');
        plot_state_constraints(ax, sim, plot_options);
    end
    
    if plot_options.control == 1
        ax = subplot(px,1,plot_options.state+1); box(ax, 'on'); hold(ax, 'off');
        plot_control_trajectory(ax, sim, plot_options); hold(ax, 'on');
    end
    
    if plot_options.disturbance == 1
        ax = subplot(px,1,plot_options.state+plot_options.control+1); hold(ax, 'off');
        plot_disturbance_trajectory(ax, sim, plot_options); hold(ax, 'on');
    end
    
    % plot of optimal trajectory
    if plot_options.optimal_trajectory
        if plot_options.state == 1
            ax = subplot(px,1,1); box(ax, 'on'); hold(ax, 'on');
            plot_state_trajectory(ax, sim_opt, plot_options_opt);
        end

        if plot_options.control == 1
            ax = subplot(px,1,plot_options.state+1); hold(ax, 'on');
            plot_control_trajectory(ax, sim_opt, plot_options_opt);
        end

        if plot_options.disturbance == 1
            ax = subplot(px,1,plot_options.state+plot_options.control+1); hold(ax, 'on');
            plot_disturbance_trajectory(ax, sim_opt, plot_options_opt);
        end
    end
    
    % save to file
    set(fig1, 'PaperPosition', [0 0 12 4*px]); %Position plot at left hand corner with width 5 and height 5.
    set(fig1, 'PaperSize', [12 4*px]);
    saveas(fig1, [plot_options.folder 'solution_N=' num2str(sim.options.N)], 'epsc')
end



%% closed loop cost plots
% 1. cumulative
Jcl_max = 0;
Jcl_min = 1e9;
for i = 1:length(sim_results)
    Jcl_max = max(Jcl_max, sim_results(i).J_cl(end));
    Jcl_min = min(Jcl_min, sim_results(i).J_cl(end));
end
Jcl_max = Jcl_max * 1.1;
Jcl_min = Jcl_min * 0.9;
labels = {};
for i = 1:length(sim_results)
    sim = sim_results(i);
    
    figure(fig2);
    ax2 = subplot(1, 1, 1); box(ax2, 'on'); hold(ax2, 'on');
    plot(ax2, 0:length(sim.J_cl)-1, sim.J_cl, 'linewidth', plot_options.linewidth);
    ylim([0 Jcl_max])
    xlim([0, options.L])
    xlabel('$L$','interpreter','latex'); ylabel(['$J^{cl}_{L}(0, 0, \mu_N)$'],'interpreter','latex');
    set(ax2,'TickLabelInterpreter','latex')
    labels{i} = ['N=' num2str(sim_results(i).options.N)];
    legend(labels, 'Location', 'southeast')
    set(fig2, 'PaperPosition', [0 0 11.3 7]); %Position plot at left hand corner with width 5 and height 5.
    set(fig2, 'PaperSize', [11.3 7]);
    saveas(fig2, [plot_options.folder 'cumulative_cost_N=' num2str(sim.options.N)], 'epsc')
end

if plot_options.optimal_trajectory == 1
    figure(fig2);
    ax2 = subplot(1, 1, 1); box(ax2, 'on'); hold(ax2, 'on');
    plot(ax2, 0:length(sim_opt.J_cl)-1, sim_opt.J_cl, 'linewidth', plot_options_opt.linewidth, 'color', plot_options_opt.cost_color);
    ylim([0 Jcl_max])
    xlim([0, options.L])
    xlabel('$L$','interpreter','latex'); ylabel(['$J^{cl}_{L}(0, 0, \mu_N)$'],'interpreter','latex');
    set(ax2,'TickLabelInterpreter','latex')
    labels{end+1} = 'N=\infty';
    legend(labels, 'Location', 'southeast')
    set(fig2, 'PaperPosition', [0 0 11.3 7]); %Position plot at left hand corner with width 5 and height 5.
    set(fig2, 'PaperSize', [11.3 7]);
    saveas(fig2, [plot_options.folder 'cumulative_cost_opt_trajectory'], 'epsc')
end


%% 2. evolution of final value of closed loop
Ns = [];
Js = [];
N_max = 0;
N_min = 1e9;
for i = 1:length(sim_results)
    N_min = min(N_min, sim_results(i).options.N);
    N_max = max(N_max, sim_results(i).options.N);
end
for i = 1:length(sim_results)
    sim = sim_results(i);
    
    Ns = [Ns; sim.options.N];
    Js = [Js; sim.J_cl(end)];
    figure(fig3);
    ax3 = subplot(1, 1, 1); box(ax3, 'on'); hold(ax3, 'off');
    plot(ax3, Ns, Js, 'linewidth', plot_options.linewidth); hold on;
    xlim([N_min N_max])
    ylim([Jcl_min Jcl_max])
    xlabel('$N$','interpreter','latex','FontSize',25); ylabel(['$J^{cl}_{' num2str(options.L) '}(0, 0, \mu_N)$'],'interpreter','latex','FontSize',25);
    set(ax3,'TickLabelInterpreter','latex')
    set(fig3, 'PaperPosition', [0 0 11.3 7]); %Position plot at left hand corner with width 5 and height 5.
    set(fig3, 'PaperSize', [11.3 7]);
    saveas(fig3, [plot_options.folder 'closed_loop_cost_N=' num2str(sim.options.N)], 'epsc')
end

if plot_options.optimal_trajectory == 1
    figure(fig3); hold(ax3, 'on');
    plot(ax3, [N_min N_max], [sim_opt.J_cl(options.L+1) sim_opt.J_cl(options.L+1)], 'linewidth', plot_options.linewidth, 'color', plot_options_opt.cost_color)
    set(fig3, 'PaperPosition', [0 0 11.3 7]); %Position plot at left hand corner with width 5 and height 5.
    set(fig3, 'PaperSize', [11.3 7]);
    saveas(fig3, [plot_options.folder 'closed_loop_cost_opt_trajectory'], 'epsc')
end

%% trajectory convergence
labels = {};
diff_max = 0;
for i = 1:length(sim_results)
    x_diff = abs(sim_results(i).xs(1:options.L+1) - sim_opt.xs(1:options.L+1));
    diff_max = max(diff_max, max(x_diff));
end
diff_max = diff_max * 1.1;
for i = 1:length(sim_results)
    sim = sim_results(i);
    
    figure(fig4);
    ax4 = subplot(1, 1, 1); box(ax4, 'on'); hold(ax4, 'on');
    x_diff = abs(sim.xs(1:options.L+1) - sim_opt.xs(1:options.L+1));
    plot(ax4, 0:options.L, x_diff, 'linewidth', plot_options.linewidth);
    xlim([0, options.L])
    ylim([0, diff_max])
    xlabel('Time $k$','interpreter','latex','FontSize',25); ylabel(['$|x_{\mu_N}(k) - x^*(k)|$'],'interpreter','latex','FontSize',25);
    set(ax4,'TickLabelInterpreter','latex')
    labels{i} = ['N=' num2str(sim_results(i).options.N)];
    legend(labels, 'Location', 'northeast','FontSize',6)
    set(fig4, 'PaperPosition', [0 0 11.3 7]); %Position plot at left hand corner with width 5 and height 5.
    set(fig4, 'PaperSize', [11.3 7]);
    saveas(fig4, [plot_options.folder 'trajectory_convergence_N=' num2str(sim.options.N)], 'epsc')
end
