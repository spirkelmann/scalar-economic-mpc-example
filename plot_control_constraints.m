% Plot the control constraints to the specified axis
% Input:
%   ax  - axis to plot to
%   ts  - time instances
%
% The function looks up the state constraints for each time index k in the
% time sequence ts and plots them to the given axis handle.
function [ output_args ] = plot_control_constraints(ax, ts )
cs = [];
for k = 1:length(ts)
    lb = -2;
    ub = 2;
    cs = [cs; lb ub];
end

plot(ax, ts, cs(:,1),'color','b','linestyle','--')
plot(ax, ts, cs(:,2),'color','b','linestyle','--')
xlim([ts(1) ts(end)])
lb = min(cs(:,1));
ub = max(cs(:,2));
ylim([lb - 0.1 * (ub-lb) ub + 0.1 * (ub-lb)]);
set(gca,'TickLabelInterpreter','latex')
box on;
end

