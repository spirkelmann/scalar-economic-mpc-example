% function of the stage cost
% We consider the following stage cost:
%   l(k, x, u) = eps * (x - x_ref(k))^2  +  u^2
%
% The function evaluates the stage cost at the current time k with the
% current state x and control u.
% The parameter epsilon defines the weighting of the state penalization
% from the reference trajectory x_ref.
% The function also returns the gradient of the stage cost w.r.t. the state
% and the control (this is used in the optimization)
function [l, gradl] = stagecost(k,x,u, options)
    [lb,ub] = bounds(k);
    x_ref = 0;
	l = 1/2 .* (options.epsilon .* (x - x_ref).^2 + u.^2);
    
    gradl = [options.epsilon*(x-x_ref); u];
end 