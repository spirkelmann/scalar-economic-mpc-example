function [ ts, xs, us, ts_ol, xs_ol, us_ol, Js, J_cl ] = loop_solve( x0, t0, options )
    x = x0;
    xs = [x];
    ts = [t0];
    us = [];
    Js = [];
    
    ts_ol = [];
    xs_ol = [];
    us_ol = [];
    
    J_cl = zeros(1, options.L);
    cl_sum = 0.0;

    for k = t0:t0+options.L
        tic
        % solve optimal control problem
        [u_opt, x_opt, J] = ocp_full_discretization(k,x, options);
        toc
        % save open loop solution for plot
        t_ol = [k:k+options.N];
        xs_ol = [xs_ol; x_opt];
        ts_ol = [ts_ol; t_ol];
        us_ol = [us_ol; u_opt];

        % apply feedback to the system
        x = f(k,x,u_opt(1), options);

        us = [us; u_opt(1)];
        xs = [xs; x(1)];
        ts = [ts; k+1];
        Js = [Js; J];
        
        cl_sum = cl_sum + stagecost(k, xs(k+1), us(k+1), options);
        J_cl(1,k+1) = cl_sum;
    end
end

