% Plot the control sequence to the specified axis
% Input:
%   ax  - axis to plot to
%   ts  - time instances
%   us  - controls
function plot_control_trajectory( ax, sim, plot_options)
    ts = sim.ts;
    us = sim.us;
    
    plot_range = plot_options.range;
    color = plot_options.control_color;
    linewidth = plot_options.linewidth;
    linestyle = plot_options.linestyle;
    
    % extract plotting data
    uplot = [];
    tplot = [];
    for i = 1:length(ts)
        if ts(i) >= plot_range(1) && ts(i) <= plot_range(end)
            tplot = [tplot; ts(i)];
            uplot = [uplot; us(i)];
        end
    end
    
    plot(ax, tplot, uplot,'color',color, 'linewidth', linewidth, 'linestyle', linestyle)
    xlim(plot_range)
    ylim([-3 3])
    xlabel('Time $k$','interpreter','latex','FontSize',25); ylabel('$u(k)$','interpreter','latex','FontSize',25);
    set(gca,'TickLabelInterpreter','latex')
    box on;
end