function [ts_opt, xs_opt, us_opt, ell_opt, J_opt] = compute_optimal_trajectory(L, t0)
global free_initial_value;
free_initial_value_old = free_initial_value;
free_initial_value = 1;

[u_ol, x_ol, J] = ocp_full_discretization(t0,L,0);

ts_opt = t0:t0+L-1;
xs_opt = x_ol;
us_opt = u_ol;
J_opt = J;

ell_opt = zeros(L, 1);
for i = 1:L
    ell_opt(i) = stagecost(i-1+t0, xs_opt(i), us_opt(i));
end
    
free_initial_value = free_initial_value_old;
end

